# enable profiler - see last line :
#zmodload zsh/zprof
HISTFILE=~/.histfile
HISTSIZE=10000
SAVEHIST=10000
# Use vi mode prompt :
bindkey -v
# The following lines were added by compinstall
zstyle :compinstall filename '/home/lafleur/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

if test -e ~/.shell/rc.sh; then
    source ~/.shell/rc.sh
fi
if test -e ~/.shell/aliases.sh; then
    source ~/.shell/aliases.sh
fi
if test -e ~/.shell/message.sh; then
    source ~/.shell/message.sh
fi

setopt hist_ignore_all_dups
setopt hist_ignore_space

# Have cdr jump to most recent dir :
autoload -Uz chpwd_recent_dirs cdr add-zsh-hook
add-zsh-hook chpwd chpwd_recent_dirs
zstyle ':completion:*:*:cdr:*:*' menu selection

# If this shell is in a vte terminal like gnome-terminal, switch back to
# current directory on new terminal opening :
#if test -x /usr/lib/vte-urlencode-cwd; then
#    local pwd='~'
#    [ "$PWD" != "$HOME" ] && pwd=${PWD/#$HOME\//\~\/}
#    pwd="${pwd//[[:cntrl:]]}"
#    printf "\033]0;%s@%s %s\033\\" "${USER}" "${HOST}" "${pwd}"
#    printf "\033]7;file://%s%s\033\\" "${HOST}" "$(/usr/lib/vte-urlencode-cwd)"
#fi


# vi-mode cursor
# --------------
#
# Picked from https://github.com/woefe/vi-mode.zsh/blob/master/vi-mode.zsh
# Updates editor information when the keymap changes.
function zle-keymap-select() {
    # Update keymap variable for the prompt
    VI_KEYMAP=$KEYMAP

    # Change cursor depending on mode.
    # Block cursor in "normal" mode, Beam in insert mode.
    [[ -n "$VI_MODE_KEEP_CURSOR" ]] || if [[ "$VI_KEYMAP" == "vicmd" ]]; then
        print -n '\e[1 q'
    else
        print -n '\e[5 q'
    fi

    zle reset-prompt
    zle -R
}

# Start every prompt in insert mode
function zle-line-init() {
    zle -K viins
}

zle -N zle-line-init
zle -N zle-keymap-select
# check for 0.1s if Esc was pressed instead of the 0.4 default :
export KEYTIMEOUT=1

# Reset the cursor to block style before running applications
function _vi_mode_reset_cursor() {
    [[ -n "$VI_MODE_KEEP_CURSOR" ]] || print -n '\e[1 q'
}
autoload -U add-zsh-hook
add-zsh-hook preexec _vi_mode_reset_cursor


# Key bindings
# ------------
#
# http://zshwiki.org/home/keybindings/
# Create a zkbd compatible hash of special keys as described by terminfo ;
# to add other keys to this hash, see: man 5 terminfo
typeset -g -A key

key[Home]="${terminfo[khome]}"
key[End]="${terminfo[kend]}"
key[Insert]="${terminfo[kich1]}"
key[Backspace]="${terminfo[kbs]}"
key[Delete]="${terminfo[kdch1]}"
key[Up]="${terminfo[kcuu1]}"
key[Down]="${terminfo[kcud1]}"
key[Left]="${terminfo[kcub1]}"
key[Right]="${terminfo[kcuf1]}"
key[PageUp]="${terminfo[kpp]}"
key[PageDown]="${terminfo[knp]}"
key[Shift-Tab]="${terminfo[kcbt]}"

# setup key accordingly
[[ -n "${key[Home]}"      ]] && bindkey -- "${key[Home]}"      beginning-of-line
[[ -n "${key[End]}"       ]] && bindkey -- "${key[End]}"       end-of-line
[[ -n "${key[Insert]}"    ]] && bindkey -- "${key[Insert]}"    overwrite-mode
[[ -n "${key[Backspace]}" ]] && bindkey -- "${key[Backspace]}" backward-delete-char
[[ -n "${key[Delete]}"    ]] && bindkey -- "${key[Delete]}"    delete-char
[[ -n "${key[Up]}"        ]] && bindkey -- "${key[Up]}"        up-line-or-history
[[ -n "${key[Down]}"      ]] && bindkey -- "${key[Down]}"      down-line-or-history
[[ -n "${key[Left]}"      ]] && bindkey -- "${key[Left]}"      backward-char
[[ -n "${key[Right]}"     ]] && bindkey -- "${key[Right]}"     forward-char
[[ -n "${key[PageUp]}"    ]] && bindkey -- "${key[PageUp]}"    beginning-of-buffer-or-history
[[ -n "${key[PageDown]}"  ]] && bindkey -- "${key[PageDown]}"  end-of-buffer-or-history
[[ -n "${key[Shift-Tab]}" ]] && bindkey -- "${key[Shift-Tab]}" reverse-menu-complete

# Have delete key working in vi CMD mode :
bindkey -a '^[[3~' vi-delete-char

# Finally, make sure the terminal is in application mode, when zle is
# active. Only then are the values from $terminfo valid.
if (( ${+terminfo[smkx]} && ${+terminfo[rmkx]} )); then
	autoload -Uz add-zle-hook-widget
	function zle_application_mode_start { echoti smkx }
	function zle_application_mode_stop { echoti rmkx }
	add-zle-hook-widget -Uz zle-line-init zle_application_mode_start
	add-zle-hook-widget -Uz zle-line-finish zle_application_mode_stop
fi

# Browse commands that started like what we typed so far with the up arrow :
autoload -Uz up-line-or-beginning-search down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search

[[ -n "${key[Up]}"   ]] && bindkey -- "${key[Up]}"   up-line-or-beginning-search
[[ -n "${key[Down]}" ]] && bindkey -- "${key[Down]}" down-line-or-beginning-search

# Bind dd to deleting the line in vicmd mode, _and then_ get into insert mode.
# The default binding doesn't do the second half.
function delete_then_insert(){
  zle kill-whole-line
  zle vi-insert
}

zle -N  delete_then_insert
bindkey -M vicmd dd delete_then_insert

# If in insert or command mode, clear editing line on Ctrl-C.
# If in another mode or during a command execution, forward the signal.
TRAPINT() {
    if [[ "${KEYMAP}" = "viins" || "$KEYMAP" = "vicmd" ]]; then
        zle kill-whole-line
        zle reset-prompt
    else
        return $(( 128 + $1 ))
    fi
}


# Prompt
# ------
#
# Remove the trailing space in RPROMPT. Used for compat
# with older terminals that would span a newline.
ZLE_RPROMPT_INDENT=0

# Async solution found at https://unix.stackexchange.com/a/645949 :
async_rprompt_callback() {
    RPROMPT="$(<&$1)"
    zle reset-prompt
    zle -F $1
}
async_rprompt() {
    # At first tell the current permissions in rprompt.
    RPROMPT="$(powerline-rs --rtl --shell zsh --theme ~/.config/powerline-rs/themer-dark.theme --modules perms)"

    # Then asynchronously check the git details.
    exec {FD}< <(
        powerline-rs --rtl --shell zsh --theme ~/.config/powerline-rs/themer-dark.theme --modules perms,git,gitstage
    )

    zle -F $FD async_rprompt_callback
}

powerline_prompt() {
    PROMPT="$(powerline-rs --shell zsh --cwd-max-depth 2 --theme ~/.config/powerline-rs/themer-dark.theme --modules ssh,host,cwd,root $?)"
    # --modules ssh,host,user,cwd,perms,git,gitstage,nix-shell,root
    async_rprompt
    # Let terminals display working dir :
    echo -en "\e]2;${PWD/$HOME/~}\a"
    # Jump back to cwd on opening a new vte-terminal :
    if test "$VTE_VERSION" && test -x /usr/lib/vte-urlencode-cwd; then
        printf "\033]7;file://%s%s\033\\" "${HOST}" "$(/usr/lib/vte-urlencode-cwd)"
    fi
}

if $(which powerline-rs); then
    precmd_functions+=(powerline_prompt)
else
    if test -e ~/.shell/naia_prompt.zsh; then
        source ~/.shell/naia_prompt.zsh
    else
        promptinit
        prompt adam2
    fi
fi > /dev/null 2>&1


# Completion
# ----------
#
# Load bash completions :
#autoload -U bashcompinit
#bashcompinit
#if $(which register-python-argcomplete); then
#    eval "$(register-python-argcomplete braise)"
#fi

# Use fish-like autosuggestions :
#suggestions="/usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh"
#if test -f $suggestions; then
#    source $suggestions
#fi
#ZSH_AUTOSUGGEST_BUFFER_MAX_SIZE=20
#ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=8'


# Syntax highlighting
# -------------------

# Let zsh highlight the selected completion with double <Tab> :
zstyle ':completion:*' menu select

# Let the common denominator be highlighted :
# Taken from https://stackoverflow.com/questions/8300687/zsh-color-partial-tab-completions
zstyle -e ':completion:*:default' list-colors 'reply=("${PREFIX:+=(#bi)($PREFIX:t)(?)*==02=01}:${(s.:.)LS_COLORS}")'

# Alternatively, use zsh-syntax-highlighting :
#highlighting="/usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"
#if test -f $highlighting; then
#    source $highlighting
#fi


# Help command
# ------------
#
autoload -Uz run-help
unalias run-help
alias help=run-help

#zprof
