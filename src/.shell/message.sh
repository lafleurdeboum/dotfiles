if test -f ~/.motd
then
  source ~/.motd
else
    if test -f /etc/hostname
    then
        hostname=$(cat /etc/hostname)
        echo "Welcome on ${GREEN}${hostname}${RESET}"
    fi
fi

if test -f /sys/class/thermal/thermal_zone0/temp
then
    temperature=$(cat /sys/class/thermal/thermal_zone0/temp | cut -c -2)
    if [ "$temperature" -ge "60" ] ; then
        temp=${RED}$temperature${RESET}
    else
        temp=${GREEN}$temperature${RESET}
    fi
    #freq=$(cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq | cut -c -3)
    #gov=$(cat /sys/devices/system/cpu/cpufreq/policy0/scaling_governor)
    #echo "CPU : température ${temp}°C - fréquence ${freq}MHz"
    echo "CPU :   ${temp}°C"
fi

if test -d "/proc"
then
    availableMem=$(grep MemAvailable /proc/meminfo | awk '{print $2}')
    totalMem=$(grep MemTotal /proc/meminfo | awk '{print $2}')
    usedMem=$((100 * (totalMem - availableMem) / totalMem))
    if [ "$usedMem" -ge "90" ] ; then
        usedRAM=${RED}$usedMem${RESET}
    elif [ "$usedMem" -ge "60" ] ; then
        usedRAM=${ORANGE}$usedMem${RESET}
    else
        usedRAM=${GREEN}$usedMem${RESET}
    fi
    echo "RAM :   ${usedRAM} % used"
fi

if test -d "/sys/class/power_supply/BAT1/"; then
    battState=$(cat /sys/class/power_supply/BAT1/status | tr '[A-Z]' '[a-z]')
    battLevel=$(cat /sys/class/power_supply/BAT1/capacity)
    if [ "$battLevel" -le "10" ] ; then
        battLevel=${RED}$battLevel${RESET}
    elif [ "$battLevel" -le "30" ] ; then
        battLevel=${ORANGE}$battLevel${RESET}
    else
        battLevel=${GREEN}$battLevel${RESET}
    fi
    echo "power : ${battLevel} % ${battState}"
fi

if test -d "/proc"
then
    case $(basename "$(cat "/proc/$PPID/comm")") in
      'sshd')
        ip=$(echo $SSH_CONNECTION | awk '{print $3}')
        realm=$(echo $ip | sed -E 's#([[:digit:]]*)$#0#')
        iface=$(ip route get $realm | head -n1 | awk '{print $4}')
        echo "connected on $iface"
        ;;
      #'tmux: server')
      # ;;
    esac
fi

