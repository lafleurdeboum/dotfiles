# Aliases
#

if test "$DISPLAY"; then
    alias nv="neovide --geometry 100x40"
    alias open="xdg-open"
    alias screen-rotate="xrandr --output eDP1 --rotate"
    #alias touchscreenEnable="xinput enable $(xinput list | grep FTSC1000 | awk '{print $(NF-3)}' | sed 's/id=//')"
    #alias touchscreenDisable="xinput disable $(xinput list | grep FTSC1000 | awk '{print $(NF-3)}' | sed 's/id=//')"
    #alias winedo="sudo -u wineuser dbus-launch --exit-with-session pcmanfm /home/wineuser/"
    alias package-choose-gui=gpk-application
    alias gnome-maps-local='gnome-maps --local ~/.cache/champlain/mapbox.streets-v4'
fi

if [ -x /usr/bin/dircolors ]; then
    alias ls='ls --color=auto'
    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

alias vi='nvim'
alias sctl='sudo systemctl'
alias uctl='systemctl --user'
alias nctl='sudo networkctl'
alias rctl='sudo resolvectl'

alias cess='bat -p'
alias la='ls -A --color=always'
alias ll='ls -lh --color=always'
alias lla='ls -lhA --color=always'
alias lt='ls -t --color=always'
alias lta='ls -tA --color=always'

alias gs='git status'
alias gd='git diff'
alias gp='git pull'
alias gP='git push'
alias gR='git reset --hard'
alias gg='git graph'

alias processes='ps xawf -eo pid,user,cgroup,args | less'
function services() {
  sudo ss --listening --query=inet,packet --ipv4 --processes | sd 'users:\(\("([^"]+)",\w+=(\w+),\w+=\w+\).+$' '$1' | sd "\s+$" ""
}
alias package-update='yay -Syu && sudo pacdiff && packageCleanOrphaned'
alias package-biggest-installed="expac --timefmt='%F' '%m %w\t\t\t%n' | awk '{print(\$1,\$3)}' | sort -nr | column -t -R1 | less"
alias package-last-installed="expac --timefmt='%Y-%m-%d %T' '%l\t%n' | sort -r | less"
alias package-modified-backup-files='pacman -Qii | awk "/^MODIFIED/ {print \$2}"'
alias package-clean-orphaned='yay -R $(yay -Qdtq) || echo no orphaned packages; yay -Sc --noconfirm | grep -v \?'
alias package-modified-conf-files="pacman -Qii | awk '/^MODIFIED/ {print \$2}'"
alias package-modified-files='paccheck --md5sum --quiet'
#alias md2console="mdcat"
alias md2console="mdr"
alias rgrep='find . -type f -print0 | xargs -0 grep --color=auto'
#alias rgrep='find . -type f -print0 $(printf "! -name" $(cat .gitignore))| xargs -0 grep --color=auto'
# Found on https://unix.stackexchange.com/questions/437468/bluetooth-headset-volume-too-low-only-in-arch
alias bt-vol-up="dbus-send --print-reply --system --dest=org.bluez /org/bluez/hci0/dev_04_FE_A1_49_14_7D org.bluez.MediaControl1.VolumeUp"
alias youtube-dl-mp3="youtube-dl -x --audio-format mp3"
# Let bluetooth stand in XBox' Xpad compatible mode.
alias xpad="echo 1 | sudo tee /sys/module/bluetooth/parameters/disable_ertm"
alias dis="espeak -s 120 -vmb-fr4"
alias say="espeak -vmb-us1"
alias bark='echo "Ouah !" && paplay /usr/share/sounds/gnome/default/alerts/bark.ogg'
alias vim-profile="vim --startuptime /tmp/vim.log +qall; sed 's/[^\ ]*\ //' /tmp/vim.log | sort -n ; rm /tmp/vim.log"

alias wifi-down='sudo ip link set down dev wlan0'
alias dbus-query='dbus-send --print-reply --dest=org.freedesktop.DBus /org/freedesktop/DBus org.freedesktop.DBus.ListNames'
alias mirror='ionice -c3 rsync --delete-excluded --delete-after -aAXH --info=progress2 --no-i-r'
function llog() {
    if [ "$#" != "1" ]
    then {
        echo -e "\nUsage: $0 SYSTEMD_UNIT_NAME"
        echo "Display both $1's service's and unit's logs since last invocation."
        return 1
    } fi
    ID=$(systemctl show -p InvocationID --value $1)
    journalctl INVOCATION_ID=$ID + _SYSTEMD_INVOCATION_ID=$ID
}
function proxy() { echo "connection to localhost:8000 will be forwarded to $1:80"; ssh -L 8000:$1:80 relatif.moi; }
alias cargo-ls='for folder in $(find . -name Cargo.toml | sed s%/Cargo.toml$%%); do test -d $folder/target && du -sh $folder/target; done'

