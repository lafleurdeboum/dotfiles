" Sample function to open a popup window.
function! NetrwPopupHelp() abort
    if &ft ==# "netrw"
        let row = line('.')
        let ui = nvim_list_uis()[0]
        let thisbuf = nvim_list_bufs()[0]
        let line1 = "gh to toggle hidden files"
        let line2 = "s to sort by name/time/size/extension, r to reverse sort"
        let line3 = "R to rename, D to delete, % to create file"
        let buf = nvim_create_buf(v:false, v:true)
        call nvim_buf_set_lines(buf, 0, 0, 0, [line1, line2, line3])
        let winopts = { 'relative' : 'win',
                      \ 'width' : ui.width,
                      \ 'height' : 3,
                      \ 'col' : 0,
                      \ 'row' : ui.height + 3,
                      \ 'anchor' : 'SW',
                      \ 'style' : 'minimal',
                      \ 'border' : 'single'
                      \ }
        let handle = nvim_open_win(buf, 1, winopts)
        call nvim_buf_set_keymap(buf, 'n', 'q', ':close<CR>:'.row.'<CR>', {'silent': v:true, 'nowait': v:true, 'noremap': v:true})
    endif
endfunction

nnoremap <silent> ? :call NetrwPopupHelp()<CR>

