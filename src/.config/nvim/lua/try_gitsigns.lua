
local gitsigns_loaded, gitsigns = pcall(function() return require('gitsigns') end)
if gitsigns_loaded
then
    gitsigns.setup({})
end
