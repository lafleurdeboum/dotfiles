local lsp_status_loaded, lsp_status = pcall(function() return require('lsp-status') end)
if lsp_status_loaded then
    lsp_status.register_progress()
end

local rust_tools_loaded, rust_tools = pcall(function() return require('rust-tools') end)
if rust_tools_loaded then
    rust_tools.setup({})
end

local signs = { Error = " ", Warning = " ", Hint = " ", Information = " " }

for type, icon in pairs(signs) do
    local hl = "LspDiagnosticsSign" .. type
    vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = "" })
end

-- Useful shortcuts.
local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end

-- Tweak when and what is showed concerning diagnostics.
vim.lsp.handlers["textDocument/publishDiagnostics"] = vim.lsp.with(
    vim.lsp.diagnostic.on_publish_diagnostics, {
        virtual_text = {
            spacing = 2,
            severity_limit = "Error"
        },
        --virtual_text = false,
        signs = true,
        update_in_insert = false,
        underline = true,
    }
)

Open_docked_preview = function(contents, syntax, opts)
    local buf = vim.api.nvim_create_buf(false, true)
    local handle = vim.api.nvim_open_win(buf, false,
        { relative='win', anchor='SW', width=80, height=3, bufpos={ 0,0 }, })
end


On_next = function()
    --vim.lsp.diagnostic.goto_next({ enable_popup = vim.lsp.util.open_floating_preview })
    vim.lsp.diagnostic.goto_next({ popup_opts = {
        border = "single",
        relative = "win",
        --offset_x = vim.fn.winwidth(0) - 20,
        --offset_y = 0,
        --anchor = "NE",
    } })

    --vim.lsp.diagnostic.goto_next({ enable_popup = false })
    --local diag = vim.lsp.diagnostic.get_next()
    --local buf = vim.api.nvim_create_buf(false, true)
    --local handle = vim.api.nvim_open_win(buf, false,
    --    { relative='win', anchor='SW', width=80, height=3, bufpos={ 0,0 }, })
    --vim.lsp.util.open_floating_preview(diag)
end

local custom_on_attach = function(client, bufnr)
    buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')

    local opts = { noremap=true, silent=true }
    buf_set_keymap('n', 'gd', '<cmd>lua vim.lsp.buf.definition()<CR>', opts)
    buf_set_keymap('n', 'gD', '<cmd>lua vim.lsp.buf.declaration()<CR>', opts)
    buf_set_keymap('n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<CR>', opts)
    buf_set_keymap('n', 'gr', '<cmd>lua vim.lsp.buf.references()<CR>', opts)
    buf_set_keymap('n', 'gt', '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
    buf_set_keymap('n', '<C-k>', '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
    buf_set_keymap('n', '<leader>ca', '<cmd>lua vim.lsp.buf.code_action()<CR>', opts)
    buf_set_keymap('n', '<leader>wa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', opts)
    buf_set_keymap('n', '<leader>wr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', opts)
    buf_set_keymap('n', '<leader>wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', opts)
    buf_set_keymap('n', '<leader>l', '<cmd>lua vim.lsp.buf.hover()<CR>', opts)
    buf_set_keymap('n', '<leader>k', '<cmd>lua vim.lsp.diagnostic.goto_prev({ popup_opts = { border = "single" }})<CR>', opts)
    buf_set_keymap('n', '<leader>j', '<cmd>lua On_next()<CR>', opts)
    --buf_set_keymap('n', '<leader>j', '<cmd>lua vim.lsp.diagnostic.goto_next({ popup_opts = { border = "single" }})<CR>', opts)
    buf_set_keymap('n', '<leader>q', '<cmd>lua vim.lsp.diagnostic.set_loclist()<CR>', opts)
    buf_set_keymap('n', '<leader>f', '<cmd>lua vim.lsp.buf.formatting()<CR>', opts)
    lsp_status.on_attach(client, bufnr)
end

local nvim_lsp_loaded, nvim_lsp = pcall(function() return require('lspconfig') end)

local servers = { 'clangd', 'vimls', 'pylsp', 'bashls', 'cssls', 'html', 'tsserver', 'vuels' }

if nvim_lsp_loaded then
    for _, lsp in ipairs(servers) do
        nvim_lsp[lsp].setup {
            on_attach = custom_on_attach,
            capabilities = lsp_status.capabilities,
            flags = {
                debounce_text_changes = 150,
            }
        }
    end

    -- language-specific configurations :
    nvim_lsp.rust_analyzer.setup {
        on_attach = custom_on_attach,
        capabilities = lsp_status.capabilities,
        settings = {
            ["rust_analyzer"] = {
                updates = {
                    channel = "nightly"
                },
                checkOnSave = {
                    enable = false
                }
            }
        }
    }

    --nvim_lsp.rls.setup {
    --    cmd = {"rustup", "run", "nightly", "rls"},
    --    on_attach = custom_on_attach,
    --    capabilities = lsp_status.capabilities,
    --    settings = {
    --        rust = {
    --            unstable_features = true,
    --            build_on_save = false,
    --            all_features = true,
    --        },
    --    },
    --}

    nvim_lsp.taplo.setup {
        cmd = { '/usr/bin/taplo-lsp', 'run' },
        on_attach = custom_on_attach,
        capabilities = lsp_status.capabilities,
    }
    nvim_lsp.sumneko_lua.setup {
        cmd = { '/usr/bin/lua-language-server', '-E', '/usr/share/lua-language-server' },
        on_attach = custom_on_attach,
        capabilities = lsp_status.capabilities,
        settings = {
            Lua = {
                telemetry = {
                    enable = false,
                },
                diagnostics = {
                    -- Get the language server to recognize the `vim` global
                    globals = {'vim'},
                },
                workspace = {
                    -- Make the server aware of Neovim runtime files
                    library = {
                        [vim.fn.expand('$VIMRUNTIME/lua')] = true,
                        [vim.fn.expand('$VIMRUNTIME/lua/vim/lsp')] = true,
                    },
                },
            },
        }
    }

    --nvim_lsp.clangd.setup {
    --    handlers = lsp_status.extensions.clangd.setup(),
    --    init_options = {
    --        clangdFileStatus = true
    --    },
    --    on_attach = custom_on_attach,
    --    capabilities = lsp_status.capabilities,
    --}
end

