set diffopt^=vertical

if &diff
    " Let differences not be underlined so they stand out
    "highlight! link DiffText MatchParen
    " Or disable syntax highlighting alltogether and highlight fg colours.
    " Not really working right now I think.
    set cursorline
    syntax off
endif

" Set foreground colors (instead of background) for diff highlighting.
"highlight DiffAdd ctermfg=196 ctermbg=none
"highlight DiffDelete ctermbg=16
"highlight DiffChange ctermfg=15 ctermbg=none
"highlight DiffText cterm=bold ctermfg=214 ctermbg=none

