" Vim syntax file
" Language: git log --oneline --graph output
" Maintainer: la Fleur <lafleur at boum.org>
" Last Change: 2021 Nov 25

" Match any of _ * | / \ <Space> at the beginning of a line any number of times.
syntax match gitGraphBefore /^[_\*|\/\\ ]\+/

" Match a word made of 7 hex chars, optionally followed by a pair of
" parenthesis holding anything but a closing parenthesis.
syntax match gitGraphRefs /\<\x\{7\}\> \(([^)]\+)\)\=/

" Match a word made of 7 hexadecimal characters :
syntax match gitGraphHash /\<\x\{7\}\>/ contained containedin=gitGraphRefs

" Match a pair of parenthesis holding anything but a closing parenthesis.
syntax match gitGraphBranches /([^)]\+)/ contained containedin=gitGraphRefs

" The same match, without a slash into it.
syntax match gitGraphLocalBranch /\w[^,)]\+/ contained containedin=gitGraphBranches

" Match the HEAD keyword inside git graph refs.
syntax match gitGraphHead /HEAD ->/ contained containedin=gitGraphBranches

" Match a "tag: TAGNAME" key in git graph refs, where TAGNAME is any string
" excepted , and ) .
syntax match gitGraphTag /tag: \S[^,)]\+/ contained containedin=gitGraphBranches

" Match '(HEAD)' - signals a detached HEAD outside of any branch tip.
syntax match gitGraphDetachedHead /(\zsHEAD\ze)/ contained containedin=gitGraphBranches

" A distant ref is the name of a distant branch, thus holding a slash inside -
" match a group of any chars containing a slash but no , and no ) .
syntax match gitGraphRemoteBranch /\w\+\/\S[^,)]\+/ contained containedin=gitGraphBranches

" Match a stash ref : instead of a branch ref, the keyword "refs/stash".
syntax match gitGraphStashRef /\w\+\/stash/ contained containedin=gitGraphBranches


highlight def link gitGraphBefore           Comment
highlight def link gitGraphHash             gitHash
highlight def link gitGraphDetachedHead     WarningMsg
highlight def link gitGraphBranches         gitHash
highlight def link gitGraphHead             WarningMsg
highlight def link gitGraphTag              gitDate
highlight def link gitGraphLocalBranch      gitKeyword
highlight def link gitGraphRemoteBranch     gitReference
highlight def link gitGraphStashRef         gitStage

