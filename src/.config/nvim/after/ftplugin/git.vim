if exists("b:did_gitgraph")
    finish
endif
let b:did_gitgraph=1

" Update the results of the GitGraph function in
" ~/.config/nvim/after/plugin/fugitive.vim .
function! UpdateGraph() abort
    if &ft =~ 'git'
        if exists("b:gitgraph")
            set modifiable
            normal! ggdG
            :0!git log --all --decorate --oneline --graph
            set nomodifiable
        endif
    endif
endfunction

augroup GitGraph
    autocmd!
    autocmd User FugitiveChanged execute UpdateGraph()
augroup END
"
