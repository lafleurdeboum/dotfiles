" Auto-raise the popup after the cursor has stayed on an error - VERY disturbing.
"au CursorHold * lua vim.lsp.diagnostic.show_line_diagnostics{focusable=false}

" highlight configuration.
" See https://www.reddit.com/r/neovim/comments/l00zzb/improve_style_of_builtin_lsp_diagnostic_messages/
highlight LspDiagnosticsVirtualTextError guifg=Red ctermfg=Red
highlight LspDiagnosticsVirtualTextWarning guifg=Yellow ctermfg=Yellow
highlight LspDiagnosticsVirtualTextInformation guifg=White ctermfg=White
highlight LspDiagnosticsVirtualTextHint guifg=White ctermfg=White
" Underline the offending code
highlight LspDiagnosticsUnderlineError guifg=NONE ctermfg=NONE cterm=underline gui=underline
highlight LspDiagnosticsUnderlineWarning guifg=NONE ctermfg=NONE cterm=underline gui=underline
highlight LspDiagnosticsUnderlineInformation guifg=NONE ctermfg=NONE cterm=underline gui=underline
highlight LspDiagnosticsUnderlineHint guifg=NONE ctermfg=NONE cterm=underline gui=underline
" Have colours in the gutterline's icons
highlight LspDiagnosticsSignError guifg=Red ctermfg=Red
highlight LspDiagnosticsSignWarning guifg=Yellow ctermfg=Yellow

