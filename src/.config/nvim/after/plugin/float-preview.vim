" Disable completeopt preview, because float-preview.nvim has its own (better) preview.
set completeopt=menuone
"autocmd WinEnter <buffer> if pumvisible() | pclose | endif
"let g:float_preview#docked = 1

