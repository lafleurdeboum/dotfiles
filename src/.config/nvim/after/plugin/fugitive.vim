" Fugitive plugin shorcuts
"

" Bring the git menu up, replacing the current buffer.
nnoremap <leader>ff :execute ':edit' . FugitiveGitDir() . '/index'<CR>

" Bring the git left menu and make it half wider.
"nnoremap <leader>fs :call PushToLeft('G')<CR>
nnoremap <leader>fs :vertical leftabove Git<CR>
" Stage file.
nnoremap <leader>fa :Gadd %<CR>
" Commit changes.
nnoremap <leader>fc :vertical Gcommit<CR>

nnoremap <leader>fd :Gvdiffsplit 
" Merging options. The preferred way is DiffConflicts.
nnoremap <leader>fm :DiffConflicts<CR>

" A fugitive git graph
function! GitGraph() abort
    Git log --all --decorate --oneline --graph
    let b:gitgraph=1
    set filetype=git
endfunction

nnoremap <silent> <leader>fg :execute GitGraph()<CR>

" Option 0 : start a git merge conflict resolution as a 3 pane diff.
"nnoremap <leader>gd :Gvdiffsplit!<CR>
" Merge from left pane in Gdiffsplit. Note that you can 1do in vimdiff.
"nnoremap gdh :diffget //2<CR> :diffupdate<CR>
" Merge from right pane.
"nnoremap gdl :diffget //3<CR> :diffupdate<CR>

