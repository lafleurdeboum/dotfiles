" nvim-gtk specific initailization :
if exists('g:GtkGuiLoaded')
  "colo base16-adwaita
  set guifont=FiraCode\ Nerd\ Font\ 12.5
  let g:GuiInternalClipboard = 1
  " Use floating cmdline :
  call rpcnotify(1, 'Gui', 'Option', 'Cmdline', 1) 
endif

