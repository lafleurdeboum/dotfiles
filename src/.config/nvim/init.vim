" Source some defaults. On Arch this is done in /usr/share/nvim/archlinux.vim
"
"set runtimepath^=~/.vim runtimepath+=~/.vim/after
"let &packpath = &runtimepath
"set runtimepath+=/usr/share/vim/vimfiles
"source ~/.vimrc
"source /usr/share/vim/vimfiles/archlinux.vim

runtime defaults.vim


" Behaviour tweaks
"

set mouse=a

" Restore the last cursor position - see :help last-position-jump
autocmd BufReadPost *
  \ if line("'\"") >= 1 && line("'\"") <= line("$") && &ft !~# 'commit'
  \ |   exe "normal! g`\""
  \ | endif

" Split on the right side :
"set splitright

" Include matching uppercase words with lowercase search term
"set ignorecase
" Include only uppercase words with uppercase search term
set smartcase
" Traverse line breaks with arrow keys
"set whichwrap=b,s,<,>,[,]

" Deal with the Esc press timeout; also impacts ??
"set timeoutlen=100

if has('vms')
  set nobackup
else
  "set backup       " keep a backup file (restore to previous version).
  if has('persistent_undo')
    set undofile    " keep an undo file (undo changes after closing).
  endif
endif

" have spell check - can be tiring if you parse code.
"set spell

" Save position of the whole vim window :
set sessionoptions^=winpos

" Set search highlight options.
let g:indexed_search_dont_move=1


" Indentation
"
" Always set autoindenting on :
set autoindent
filetype plugin indent on
" show existing tab with this many spaces width :
set tabstop=4
" when indenting with '>', use this number of spaces width :
set shiftwidth=4
" On pressing tab, insert this same number of spaces :
set expandtab


" Command tweaks
"
let mapleader = ' '

" Use Y to yank to system clipboard.
vnoremap Y "+ygv
" Move text and rehighlight -- vim tip_id=224 :
vnoremap > ><CR>gv
vnoremap < <<CR>gv
" Let <Shift-u> redo because I never remember ctrl-r mapping.
nnoremap U <C-r>
" Let <Shift-t> jump to tag because ctr-alt-) is so bad on french keyboards.
nnoremap T <C-]>
" Esc key twice will exit insert mode in terminal :
tnoremap <Esc><Esc> <C-\><C-n>

" Change local dir to that of the local file. See :help filename-modifiers .
nnoremap <leader>cd :lcd %:p:h<CR>
" Switch line numbering.
nnoremap <C-n> :set invnumber<CR>

" Jump one character right while in insert mode (escape from parens).
inoremap <C-l> <Esc>la

" Reload nvim init script.
nnoremap <leader>sv :source $MYVIMRC<cr>

" Write the file with sudo. TODO reload the file when it is done.
command W :execute ':silent w !sudo tee > /dev/null "%"' | :edit!

" Build a tab completion function. TODO behave better when there is a space on
" the left and a character on the right.
function SmartTabComplete()
    let line = getline('.')                         " current line
    let substr = strpart(line, -1, col('.')+1)      " from the start of the current
                                                    " line to one character right
                                                    " of the cursor
    let substr = matchstr(substr, "[^ \t]*$")       " word till cursor
    if (strlen(substr)==0)                          " nothing to match on empty string
        return "\<tab>"
    elseif pumvisible()                             " floating window is open
        return "\<C-n>"                             " step to next suggestion
    else
        return "\<C-x>\<C-o>"                       " display completions
    endif
endfunction

inoremap <Tab> <C-r>=SmartTabComplete()<Cr>


" Presentation
"
colorscheme ThemerVim
" Break lines at word (requires Wrap lines) :
set linebreak
" Wrap-broken line prefix - !!! followed by a '\ ' intentionally !
set showbreak=+++\ 

" Add ligatures support in GUI - this should appear as 1 character : =>
set guifont=FiraCode\ Nerd\ Font:h14
" Using a fallback - test with :set guifont=...
"set guifont=FiraCode\ Nerd\ Font,TerminessTTF\ Nerd\ Font:h19
"set guifont=FiraCode\ Nerd\ Font,Fira\ Mono\ for\ Powerline:h19


" Neovide specific config
"
" neovide doesn't load ginit.vim, but others guis go there.
if exists('g:neovide')
    "guifont size syntax is not the same in neovide as in other GUIs.
    ""set guifont=FiraCode\ Nerd\ Font:h7.45
    set guifont=FiraCode\ Nerd\ Font:h12.5
    let g:neovide_cursor_trail_length=0.8
    let g:neovide_cursor_vfx_mode = "sonicboom"
    let neovide_remember_window_size = v:true
endif


" Load lsp config from ./lua/lsp.lua. It defines keymaps and such.
lua require('lsp')

" Use gitsigns, a replacement to gitgutter.
lua require('try_gitsigns')

" Attempt to have LspDiags docked by FloatPreview. Quite worthless in fact.
"au User LspDiagnosticsChanged call float_preview#_menu_popup_changed()
"au BufWinEnter * call float_preview#reopen()
"au BufWinEnter * :echo "ok"

