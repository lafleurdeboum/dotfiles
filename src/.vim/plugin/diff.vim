set diffopt^=vertical

if &diff
    " Let differences not be underlined so they stand out
    "highlight! link DiffText MatchParen
    " Or disable syntax highlighting alltogether and highlight fg colours.
    " Not really working right now I think.
    set cursorline
    syntax off
endif

" Set foreground colors (instead of background) for diff highlighting.
hi DiffAdd ctermfg=196 ctermbg=none
hi DiffDelete ctermbg=16
hi DiffChange ctermfg=15 ctermbg=none
hi DiffText cterm=bold ctermfg=214 ctermbg=none

