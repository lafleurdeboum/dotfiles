" Vim syntax file
" Language: git log --oneline --graph output
" Maintainer: la Fleur <lafleur at boum.org>
" Last Change: 2021 Nov 25

" Match any of _ * | / \ <Space> at the beginning of a line any number of times.
syntax match gitGraphBefore /^[_\*|\/\\ ]\+/

" Match a word made of 7 hexadecimal characters :
syntax match gitGraphHash /\(\<\x\{7\}\>\)/

" Match a parenthesis, any character but the closing paren any number of
" times but at least once, and a closing paren, _only_ following a gitGraphHash.
syntax match gitGraphRefs /\<\x\{7\}\> \(([^)]\+)\)/ contains=gitGraphHash

" Match the HEAD keyword inside git graph refs.
syntax match gitGraphHead /HEAD/ contained containedin=gitGraphRefs
" Match a "tag: TAGNAME" key in git graph refs, where TAGNAME is any string
" excepted , and ) .
syntax match gitGraphTag /tag: \S[^,)]\+/ contained containedin=gitGraphRefs

" A distant ref is the name of a distant branch, thus holding a slash inside -
" match a group of any chars containing a slash but no , and no ) .
syntax match gitGraphDistantBranch /\w\+\/\S[^,)]\+/ contained containedin=gitGraphRefs


highlight def link gitGraphBefore           Comment
"highlight def link gitGraphDate             gitDate
highlight def link gitGraphHash             gitHash

"highlight def link gitGraphRefs             gitReference
highlight def link gitGraphRefs             gitKeyword
highlight def link gitGraphHead             WarningMsg
highlight def link gitGraphTag              gitDate
highlight def link gitGraphDistantBranch    gitReference

