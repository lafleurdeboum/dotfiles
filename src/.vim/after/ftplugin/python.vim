if exists("g:ale_enabled")
  "let b:ale_linters = {'python': ['pyright']}
  "let b:ale_linters = {'python': ['pylint']}
  let b:ale_linters = {'python': ['pyls']}
  let b:ale_python_pylint_options = '--disable=trailing-newlines'
endif
