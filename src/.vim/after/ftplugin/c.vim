if exists("g:ale_enabled")
  "let b:ale_linters = {'c': ['clang']}
  let b:ale_linters = {'c': ['clangd']}
  "let g:ale_linters_explicit=1
  "let b:ale_c_always_make = 0
  let b:ale_c_parse_compile_commands = 1
  let b:ale_c_parse_makefile = 1
  " We can use bear to build a compile_commands.json that will tell what to
  " include when linting :
  "set makeprg=bear\ --\ make
endif
