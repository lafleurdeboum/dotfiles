" Sort items by their apparition order.
let g:tagbar_sort = 0

function! OpenTags() abort
    " Update the width to a third of the window but at least 25.
    let g:tagbar_width = max([25, winwidth(0) / 3])
    :TagbarOpen fjc
endfunction

nnoremap <leader>t :silent call OpenTags()<CR>

