" My conf file for vim-mergetool. See also .../plugin/airline.vim .
" Quit function doing a clean mergetool stop.
if get(g:, 'mergetool_in_merge_mode', 0)
  function s:QuitWindow()

    call mergetool#stop()
    quit
  endfunction

  command QuitWindow call s:QuitWindow()
  nnoremap <silent> <leader>q :QuitWindow<CR>
endif


