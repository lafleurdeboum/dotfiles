" Replace ]c and [c shortcuts - you should already have <Leader>hs and
" <Leader>hu to (un)stage a hunk.
"nnoremap <Leader>hh :GitGutterNextHunk<CR>
nnoremap <Leader>hh <Plug>(GitGutterNextHunk)
nnoremap <Leader>hH <Plug>(GitGutterPrevHunk)

" Do not clobber other processes' signs :
let g:gitgutter_set_sign_backgrounds = 1

"let g:gitgutter_sign_added = 'xx'
"let g:gitgutter_sign_modified = 'yy'
"let g:gitgutter_sign_removed = 'zz'
"let g:gitgutter_sign_removed_first_line = '^^'
"let g:gitgutter_sign_removed_above_and_below = '{'
"let g:gitgutter_sign_modified_removed = 'ww'

