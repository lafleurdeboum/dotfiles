" Fugitive plugin shorcuts
"
" Bring the git left menu and make it half wider.
"nnoremap <leader>fs :call PushToLeft('G')<CR>
nnoremap <leader>fs :vertical leftabove G<CR>
" Stage file.
nnoremap <leader>fa :Gadd %<CR>
" Commit changes.
nnoremap <leader>fc :vertical Gcommit<CR>

nnoremap <leader>fd :Gvdiffsplit 
" Merging options. The preferred way is DiffConflicts.
nnoremap <leader>fm :DiffConflicts<CR>

" A fugitive git graph
"command -nargs=* Glg Git! lg <args>

" Preferred option : my own git graph :
nnoremap <leader>fg :Git graph<CR>

" Option 0 : start a git merge conflict resolution as a 3 pane diff.
"nnoremap <leader>gd :Gvdiffsplit!<CR>
" Merge from left pane in Gdiffsplit. Note that you can 1do in vimdiff.
"nnoremap gdh :diffget //2<CR> :diffupdate<CR>
" Merge from right pane.
"nnoremap gdl :diffget //3<CR> :diffupdate<CR>

