" deoplete completion options.
"
" Note that deoplete needs to be activated in ~/.vimrc
"
" Sources are : 
" - ale - though I never saw it in the completion sources.
" - omni - same remark.
" - racer : /usr/bin/racer rust linter through vim-racer plugin
" - jedi : python-jedi through the deoplete-jedi vim plugin
" - syntax : use vim syntax files for various filetypes through
"   the neco-syntax plugin
" - vim : vim own syntax, through the neco-vim plugin
"  \ 'sources': { '_': ['ale'] },

if exists('g:loaded_deoplete')
  call deoplete#custom#option({
  \ 'auto_complete_delay': 200,
  \ 'sources': { '_': ['ale', 'omni', 'racer', 'jedi', 'syntax', 'vim'] },
  \ })
  call deoplete#custom#source('ale', 'rank', 1001)
  " Quit the preview window when leaving insert mode :
  augroup Deoplete
    autocmd CompleteDone * silent! pclose!
  augroup END
  " Disable preview window on completion :
  "set completeopt-=preview
endif

" options for vim-racer completion plugin, used by deoplete :
let g:racer_experimental_completer = 1
let g:racer_insert_paren = 1
augroup Racer
    autocmd!
    autocmd FileType rust nnoremap <buffer> gd         <Plug>(rust-def)
    autocmd FileType rust nnoremap <buffer> gs         <Plug>(rust-def-split)
    autocmd FileType rust nnoremap <buffer> gx         <Plug>(rust-def-vertical)
    autocmd FileType rust nnoremap <buffer> gt         <Plug>(rust-def-tab)
    autocmd FileType rust nnoremap <buffer> <leader>gd <Plug>(rust-doc)
    autocmd FileType rust nnoremap <buffer> <leader>gD <Plug>(rust-doc-tab)
augroup END

