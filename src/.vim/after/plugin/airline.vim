if exists('g:airline')
    :scriptencoding utf-8
    " Let vim-mergetool show merge mode
    function! AirlineDiffmergePart()
      if get(g:, 'mergetool_in_merge_mode', 0)
          return '↸'
      endif

      if &diff
          return '↹'
      endif

      return ''
    endfunction

    call airline#parts#define_function('_diffmerge', 'AirlineDiffmergePart')
    call airline#parts#define_accent('_diffmerge', 'bold')

    " Don't show file type :
    :let g:airline_section_x = ''
    " Don't show file encoding :
    :let g:airline_section_y = ''
    " Toggle tagbar integration (showing current function) :
    :let g:airline#extensions#tagbar#enabled = 1

    " Add a section with vim-mergetool status
    " Do not display total number of lines, just the current one.
    :let g:airline_section_z = airline#section#create(['_diffmerge', '%3p%% %#__accent_bold#%{g:airline_symbols.linenr}%l%#__restore__#:%v'])

    let g:airline_theme='base16_adwaita'

    " ALE should print warnings in the cmd line :
    let g:airline#extensions#ale#enabled = 1

    " neovide doesn't fully support Powerline fonts but does ligature fonts, use
    " that instead :
    if exists('g:neovide')
        let g:airline_symbols.linenr = '->'
        let g:airline_left_sep = ''
        let g:airline_right_sep = ''
    endif
endif

