" Find a cscope db in cscope folder as well as in cwd.
if has('cscope')
    if has('vms')
        if filereadable('.git/cscope/cscope.out')
            cs add .git/cscope/cscope.out
        endif
    else
        if filereadable('cscope/cscope.out')
            cs add cscope/cscope.out
        endif
        if filereadable('cscope.out')
            cs add cscope.out
        endif
    endif
endif

