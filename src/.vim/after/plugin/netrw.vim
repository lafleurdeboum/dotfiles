" Netrw file browser
"
" Let netrw list files tree-like :
let g:netrw_liststyle = 4

" netrw will :
" 0 - open files in current window
" 1 - open files in a new horizontal split
" 2 - open files in a new vertical split
" 3 - open files in a new tab
" 4 - open in previous window
let g:netrw_browse_split = 0
let g:netrw_winsize = &columns / 5

" Let netrw hide dotfiles :
let g:netrw_list_hide = '^\..'

