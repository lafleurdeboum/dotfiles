

  
  if &background == 'dark'
    
  let s:guishade0 = "#282629"
  let s:guishade1 = "#474247"
  let s:guishade2 = "#656066"
  let s:guishade3 = "#847e85"
  let s:guishade4 = "#a29da3"
  let s:guishade5 = "#c1bcc2"
  let s:guishade6 = "#e0dce0"
  let s:guishade7 = "#fffcff"
  let s:guiaccent0 = "#ff4050"
  let s:guiaccent1 = "#f28144"
  let s:guiaccent2 = "#ffd24a"
  let s:guiaccent3 = "#a4cc35"
  let s:guiaccent4 = "#26c99e"
  let s:guiaccent5 = "#66bfff"
  let s:guiaccent6 = "#cc78fa"
  let s:guiaccent7 = "#f553bf"
  let s:shade0 = 235
  let s:shade1 = 238
  let s:shade2 = 241
  let s:shade3 = 244
  let s:shade4 = 247
  let s:shade5 = 250
  let s:shade6 = 253
  let s:shade7 = 231
  let s:accent0 = 204
  let s:accent1 = 215
  let s:accent2 = 221
  let s:accent3 = 149
  let s:accent4 = 79
  let s:accent5 = 117
  let s:accent6 = 177
  let s:accent7 = 212
  
  endif
  

  
  if &background == 'light'
    
  let s:guishade0 = "#fffcff"
  let s:guishade1 = "#e0dce0"
  let s:guishade2 = "#c1bcc2"
  let s:guishade3 = "#a29da3"
  let s:guishade4 = "#847e85"
  let s:guishade5 = "#656066"
  let s:guishade6 = "#474247"
  let s:guishade7 = "#282629"
  let s:guiaccent0 = "#f03e4d"
  let s:guiaccent1 = "#f37735"
  let s:guiaccent2 = "#eeba21"
  let s:guiaccent3 = "#97bd2d"
  let s:guiaccent4 = "#1fc598"
  let s:guiaccent5 = "#53a6e1"
  let s:guiaccent6 = "#bf65f0"
  let s:guiaccent7 = "#ee4eb8"
  let s:shade0 = 231
  let s:shade1 = 253
  let s:shade2 = 250
  let s:shade3 = 247
  let s:shade4 = 244
  let s:shade5 = 241
  let s:shade6 = 238
  let s:shade7 = 235
  let s:accent0 = 204
  let s:accent1 = 209
  let s:accent2 = 221
  let s:accent3 = 149
  let s:accent4 = 79
  let s:accent5 = 110
  let s:accent6 = 177
  let s:accent7 = 212
  
  endif
  

  let s:p = {'normal': {}, 'inactive': {}, 'insert': {}, 'replace': {}, 'visual': {}, 'tabline': {}}
  let s:p.normal.left = [ [ s:guishade1, s:guiaccent5, s:shade1, s:accent5 ], [ s:guishade7, s:guishade2, s:shade7, s:shade2 ] ]
  let s:p.normal.right = [ [ s:guishade1, s:guishade4, s:shade1, s:shade4 ], [ s:guishade5, s:guishade2, s:shade5, s:shade2 ] ]
  let s:p.inactive.right = [ [ s:guishade1, s:guishade3, s:shade1, s:shade3 ], [ s:guishade3, s:guishade1, s:shade3, s:shade1 ] ]
  let s:p.inactive.left =  [ [ s:guishade4, s:guishade1, s:shade4, s:shade1 ], [ s:guishade3, s:guishade0, s:shade3, s:shade0 ] ]
  let s:p.insert.left = [ [ s:guishade1, s:guiaccent3, s:shade1, s:accent3 ], [ s:guishade7, s:guishade2, s:shade7, s:shade2 ] ]
  let s:p.replace.left = [ [ s:guishade1, s:guiaccent1, s:shade1, s:accent1 ], [ s:guishade7, s:guishade2, s:shade7, s:shade2 ] ]
  let s:p.visual.left = [ [ s:guishade1, s:guiaccent6, s:shade1, s:accent6 ], [ s:guishade7, s:guishade2, s:shade7, s:shade2 ] ]
  let s:p.normal.middle = [ [ s:guishade5, s:guishade1, s:shade5, s:shade1 ] ]
  let s:p.inactive.middle = [ [ s:guishade4, s:guishade1, s:shade4, s:shade1 ] ]
  let s:p.tabline.left = [ [ s:guishade6, s:guishade2, s:shade6, s:shade2 ] ]
  let s:p.tabline.tabsel = [ [ s:guishade6, s:guishade0, s:shade6, s:shade0 ] ]
  let s:p.tabline.middle = [ [ s:guishade2, s:guishade4, s:shade2, s:shade4 ] ]
  let s:p.tabline.right = copy(s:p.normal.right)
  let s:p.normal.error = [ [ s:guiaccent0, s:guishade0, s:accent0, s:shade0 ] ]
  let s:p.normal.warning = [ [ s:guiaccent2, s:guishade1, s:accent2, s:shade1 ] ]

  let g:lightline#colorscheme#ThemerVimLightline#palette = lightline#colorscheme#fill(s:p)

  