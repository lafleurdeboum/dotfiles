# A simple themer script

## Semantic colours
Colours are used in modern programs to convey meaning - eg red means something
went wrong - that's what "semantic colours" is all about. Since the first time
there was more than two colours on a screen, there has been _many_ different
standards to give means to use colours, particularily in CLIs, making the
subject subtly but deeply complicated to get right.

Of course, programs use different nuances of eg red to tell that something
went wrong. This not only feels bad, but hinders the conveyed meaning. To
harmonize the colours conveying meaning, there has been plenty of initiatives.
This repository uses `themer`.

## Installation
To generate and copy over the colour configuration files, run
```
yarn install
yarn build
./install-in-dotfiles.sh
```
Run the install script or see the instructions in `build/README.md`. Some are
lacking, like themer-gnome-terminal, that might work, and needs
```
./build/themer/themer-gnome-terminal/gnome-terminal-dark-install.sh default
```
If you forget the "default" argument, it should complain (about something else).

To update
```
yarn upgrade --latest
yarn build
./install-in-dotfiles.sh
```

To clean
```
rm -r build node_modules yarn.lock
```

