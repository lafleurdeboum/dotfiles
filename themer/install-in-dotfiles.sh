grep -q colors build/alacritty/Themer.yml || echo "colors: *dark" >> build/alacritty/Themer.yml
install -m 0644 build/alacritty/Themer.yml ../src/.config/alacritty/Themer.yml
install -m 0644 build/kitty/themer-dark.conf ../src/.config/kitty/themer-dark.conf
install -m 0644 build/kitty/themer-light.conf ../src/.config/kitty/themer-light.conf
install -m 0644 build/themer-powerline-rs/themer-dark.theme ../src/.config/powerline-rs/themer-dark.theme
install -m 0644 build/themer-powerline-rs/themer-light.theme ../src/.config/powerline-rs/themer-light.theme
install -m 0644 build/vim/ThemerVim.vim ../src/.vim/colors/ThemerVim.vim
install -m 0644 build/vim-lightline/ThemerVimLightline.vim ../src/.vim/autoload/lightline/colorscheme/ThemerVimLightline.vim
install -m 0644 build/xresources/themer-dark.Xresources ../src/.Xresources-colours-dark
install -m 0644 build/xresources/themer-light.Xresources ../src/.Xresources-colours-light
