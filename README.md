# My dotfiles
Those are my dotfiles ! They are copyleft, so you don't even have to quote me
to use them. They give sensible tweaks for the following programs :
  - vim
  - neovim
  - zsh
  - bash
  - git

 All the dotfiles include harmonized colour profiles generated with `themer`,
 you don't have to do anything to use it. If you wish to change anything about
 those colours, go check the README I wrote in the [themer](./themer)
 folder.


## Installation
First
``` sh
git clone https://git.relatif.moi/lafleur/dotfiles
```
Then to symlink them all in your home folder :
```
cd dotfiles/src
stow -t ~ .
```
Stow will not do anything and warn you if a single link origin is already
existing, so you may do this safely.

You may as well pick some dotfiles :
```
cd dotfiles/src
alias lnk='ln -s --backup --suffix=.dotfiles-replaced'
lnk $(pwd)/.bashrc ~/.bashrc
lnk $(pwd)/.zshrc ~/.zshrc
lnk $(pwd)/.shell ~/.shell
lnk $(pwd)/.gitconfig ~/.gitconfig
lnk $(pwd)/.vim ~/.vim
lnk $(pwd)/.vimrc ~/.vimrc
lnk $(pwd)/.Xresources ~/.Xresources
lnk $(pwd)/.Xresources-colours-dark ~/.Xresources-colours-dark
lnk $(pwd)/nvim ~/.config/nvim
lnk $(pwd)/kitty ~/.config/kitty
lnk $(pwd)/powerline-rs ~/.config/powerline-rs
unalias lnk
```

To revert the changes, you just need to remove the relevant symlinks. Do :
```
cd dotfiles/src
stow -t ~ -D .
```
If you used the symlink technique upper, there might be leftovers, run
```
find ~ -type l -name '*.dotfiles-replaced'
```

### vim/neovim
`.vim` and `nvim` both have some submodules to pull some useful plugins. You
can pull them anytime like this :
``` sh
git submodule init [hit tab to get completion on the available submodules]
git submodule update
```
The `update` command is quite misleading ; it will in fact pull the
submodules' contents in. To update those pulled to their respective latest
version :
``` sh
git submodule foreach git pull origin master
```
Removing a git submodule has always been quite painful ; _if_ you didn't move
the submodule you may use the alias that lies in my `.gitconfig` :
``` sh
git rmsubmodule path/to/submodule
```

I use some other plugins that are provided by Arch or its AUR repository,
namely :
  - vim-fugitive
  - vim-gitgutter
  - vim-indexed-search-git
  - vim-lightline
  - vim-auto-pairs
  - vim-move-git
  - vim-sandwich
  - vim-tagbar
  - vim-mesonic
  - vim-gtk-recent-git
  - vim-illuminate (yes, it's a bit too much. There has to be one)
  - agit.vim-git
  - vim-vala-git
  - vim-rust-git
  - vim-toml-git
  - python-pynvim (needed by vim-gtk-recent-git)

Neovim now provides a language server protocol implementation. Those are the
language servers that I have installed for it to use :
  - vim-language-server
  - bash-language-server
  - python-lsp-server, a community alternative to Microsoft's python LSP
  - typescript-language-server-bin for javascript and typescript
  - vscode-css-languageserver-bin
  - vscode-html-languageserver-bin
  - yaml-languageserver-bin
  - nodejs-vls for vue.js
  - vala-language-server
  - lua-language-server-git
  - taplo-lsp for toml
  - yaml-language-server-bin
  - rust-analyzer (said to be the most on par with rustc)
  - clang for C/C++, and the amazing `bear` symbol gatherer.

### git
`.gitconfig` relies on .vim/pack/diffconflicts/start/diffconflicts . It also
states my username as a remote login, be sure to check it out before sourcing.

### zsh/bash
I must admit I didn't use my `.bashrc` lately ; the thing is, zsh really _is_
more useable.  BTW, I use
[`powerline-rs`](https://gitlab.com/jD91mZM2/powerline-rs) for a shell prompt,
it's compiled, which makes it very efficient, and it's pretty neat. It's
optional, of course ; if it's not installed you would get a nice and simple
prompt adapted from Manjaro.

`.shell` contains some sub-scripts sourcable by bashrc or zshrc, like shell
aliases and the like.

I think that's it, enjoy !

